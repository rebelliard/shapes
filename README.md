# JavaScript assignment: Shapes

## Description

The goal of this application is to find parallelograms based on three positions that a user can freely choose from the available screen region.

* Once the user selects these three positions, the application should calculate where the fourth position should be in order to create a blue parallelogram figure (a quadrilateral with two pairs of parallel sides).
* The application will also generate a yellow circle in the parallelogram's center of mass and with its same area.
* Finally, it will show all of the polygon's coordinates, as well as the value of its area.

For more information, [read the original instructions here](https://gitlab.com/rebelliard/shapes/blob/master/instructions.md).

### How to run the demo

* Decompress the `shapes--rafaeldo.zip` file.
* Navigate to the extracted `demo` folder.
* Open `index.html`.

### How to use the application

* With your pointing device, arbitrarily select three points within the large, central region of the screen.
* Once you do so, the application will generate both a blue parallelogram and a yellow circle. It will also display the calculated coordinates as well the polygon's area.
* You're able to hover over any of the 4 points of the parallelogram to move its position, while maintaining all of the geometrical properties of a parallelogram figure.
* Once you're done with a given parallelogram, you're able to hit the [Reset] button to start anew.

## About me

I'm [Rafael Belliard](https://rafael.do) and I'm a developer interested in building meaningful software that adds real value to the user's life. I wrote this small application over the course of a weekend as part of an evaluation for a job application at [Kognity](http://kognity.com/) for their [Senior Web Engineer](https://emp.jobylon.com/jobs/9103-kognity-senior-web-engineer/) position.

## Development instructions

### Technologies used

* [Pointer Events API](https://developers.google.com/web/updates/2016/10/pointer-events) (native browser support): used to manage events (e.g., click, drag, drop).
* [React](https://reactjs.org/): used at a basic level, primarily to make use of JSX.
* [Next.js](https://nextjs.org/): framework around React built to statically export the site.
* [Immutability helper](https://github.com/kolodny/immutability-helper#immutability-helper): used to ensure the array manipulations didn't interfere with React's immutable nature.
* [Sass](https://sass-lang.com/): used to setup the site using the CSS grid spec.

### Install essential global packages

* Install [git](https://git-scm.com/downloads).
* Install [node](https://nodejs.org/en/download/).
  * If you're able to, I suggest using [nvm](https://github.com/creationix/nvm).
* Install [yarn](https://yarnpkg.com/lang/en/docs/install/).

### Extract the source code

* Decompress the `shapes--rafaeldo.zip` file.
* Navigate to the extracted `src` folder.

### Install required packages

```
yarn
```

### Run the development server

```
yarn dev
```

* Navigate to [localhost:3000](http://localhost:3000/).

### Run tests

```
yarn test
```

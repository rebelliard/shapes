import {
  calculateArea,
  calculateCenterOfMass,
  calculateMissingAngle,
} from '../pages/utils/Parallelogram'

describe('Parallelogram', () => {
  describe('Missing angle', () => {
    it('should find angle #1', () => {
      const polygon = [
        undefined,
        {x: 447.78, y: 48.18},
        {x: 445.02, y: 176.53},
        {x: 352.62, y: 188.23},
      ]

      const missingIndex = 0
      const expected = {x: 355.38, y: 59.87999999999999}
      const actual = calculateMissingAngle(polygon, missingIndex)
      expect(actual).toEqual(expected)
    })

    it('should find angle #2', () => {
      const polygon = [
        {x: 355.37, y: 59.89},
        undefined,
        {x: 445.02, y: 176.53},
        {x: 352.62, y: 188.23},
      ]

      const missingIndex = 1
      const expected = {x: 447.77, y: 48.190000000000026}
      const actual = calculateMissingAngle(polygon, missingIndex)
      expect(actual).toEqual(expected)
    })

    it('should find angle #3', () => {
      const polygon = [
        {x: 355.37, y: 59.89},
        {x: 447.78, y: 48.18},
        undefined,
        {x: 352.62, y: 188.23},
      ]

      const missingIndex = 2
      const expected = {x: 445.03, y: 176.51999999999998}
      const actual = calculateMissingAngle(polygon, missingIndex)
      expect(actual).toEqual(expected)
    })

    it('should find angle #4', () => {
      const polygon = [
        {x: 355.37, y: 59.89},
        {x: 447.78, y: 48.18},
        {x: 445.02, y: 176.53},
        undefined,
      ]

      const missingIndex = 3
      const expected = {x: 352.61, y: 188.24}
      const actual = calculateMissingAngle(polygon, missingIndex)
      expect(actual).toEqual(expected)
    })
  })

  describe('Center of mass', () => {
    it('should calculate center of mass', () => {
      const polygon = [
        {x: 355.37, y: 59.89},
        {x: 447.78, y: 48.18},
        {x: 445.02, y: 176.53},
        {x: 352.62, y: 188.23},
      ]

      const expected = {x: 400.195, y: 118.205}
      const actual = calculateCenterOfMass(polygon)
      expect(actual).toEqual(expected)
    })
  })

  describe('Area', () => {
    it('should calculate area', () => {
      const polygon = [
        {x: 355.37, y: 59.89},
        {x: 447.78, y: 48.18},
        {x: 445.02, y: 176.53},
        {x: 352.62, y: 188.23},
      ]

      const expected = 108.75418359768967
      const actual = calculateArea(polygon)
      expect(actual).toEqual(expected)
    })
  })
})

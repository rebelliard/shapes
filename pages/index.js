import update from 'immutability-helper'
import React, {Component} from 'react'
import '../styles/styles.scss'
import Head from './components/Head'
import {calculateArea, calculateCenterOfMass, calculateMissingAngle} from './utils/Parallelogram'

class Canvas extends Component {
  constructor(props) {
    super(props)

    this.state = Canvas.initialState
  }

  componentDidMount() {
    /**
     * Pointer Events Polyfill for Safari.
     * @see https://github.com/jquery/PEP
     * @see https://caniuse.com/#feat=pointer
     */
    require('pepjs')
  }

  static get initialState() {
    return {
      points: [],
      circle: undefined,
      area: undefined,
      selectedIndex: undefined,
    }
  }

  /**
   * The program highlights their location by drawing red circles,
   * 11 pixels in diameter, centered on each selected point.
   */
  static get pointerDiameter() {
    return 11
  }

  /**
   * A radius can be a line from any point on the circumference to the center of the circle.
   * It can be found by dividing the diameter in half.
   * @see https://www.mathopenref.com/diameter.html
   */
  static get pointerRadius() {
    return Canvas.pointerDiameter / 2
  }

  /**
   * The pointerdown event is fired when a pointer becomes active.
   * @see https://developer.mozilla.org/en-US/docs/Web/Events/pointerdown
   */
  handleCanvasPointerDown = event => {
    // Only "points" can be moved.
    if (event.target.classList.contains('point')) {
      this.setState({
        isDragging: true,
        selectedIndex: Number(event.target.dataset.index),
      })
    } else {
      this.setPoint(event)
    }
  }

  /**
   * The pointermove event is fired when a pointer changes coordinates, and the pointer
   * has not been canceled by a browser touch-action.
   * @see https://developer.mozilla.org/en-US/docs/Web/Events/pointermove
   */
  handleCanvasPointerMove = event => {
    if (!this.state.isDragging) {
      return
    }

    this.updatePoint(event)
  }

  /**
   * The pointerup event is fired when a pointer is no longer active.
   * @see https://developer.mozilla.org/en-US/docs/Web/Events/pointerup
   */
  handleCanvasPointerUp = event => this.setState({isDragging: false})

  isPointSelected = newPoint => {
    return this.state.points.find(point => point.x === newPoint.x && point.y === newPoint.y)
  }

  /**
   * Set a new point on the canvas.
   */
  setPoint = event => {
    const point = this.getPointerCoordinates(event)

    /**
     * On some Android devices, `pointerdown` gets hit, then `touchstart`
     * repeats the same operation.
     */
    const isPointSelected = this.isPointSelected(point)
    if (isPointSelected) {
      return
    }

    /**
     * If the parallelogram is formed, remove the last index,
     * as it will be replaced by latest value.
     */
    if (this.state.points.length >= 3) {
      this.setState(prevState => {
        const points = update(prevState.points, {
          $apply: prevPoints => prevPoints.slice(0, 2),
        })

        return {points}
      })
    }

    this.setState(
      prevState => ({
        points: update(prevState.points, {
          $push: [point],
        }),
      }),
      this.setShapes,
    )
  }

  /**
   * When updating a point, we want to take into account one of its adjacent angles.
   */
  static get desiredAdjacentAngle() {
    // These indexes correspond to one of the parallelogram's 4 angles.
    return {
      '0': 1,
      '1': 0,
      '2': 3,
      '3': 2,
    }
  }

  /**
   * Update a point's coordinates and recalculate all shapes.
   */
  updatePoint = event => {
    const point = this.getPointerCoordinates(event)
    const {selectedIndex} = this.state
    const adjacentIndex = Canvas.desiredAdjacentAngle[selectedIndex]

    /**
     * If there's already a complete parallelogram in the canvas,
     * we want to recalculate its dimensions. To do so:
     *  1. We first "disable" one of its adjacent angles so that it's recalculated.
     *  2. We update the moved point's coordinates.
     *  3. We let `setShapes` do its job.
     */
    if (this.state.points.length === 4) {
      this.setState(prevState => {
        const points = update(prevState.points, {
          $apply: prevpoints => {
            prevpoints[adjacentIndex] = undefined
            return prevpoints
          },
        })
        return {points}
      })
    }

    this.setState(prevState => {
      // Insert an item into the list at the specified index.
      const points = update(prevState.points, {
        $splice: [[selectedIndex, 1, point]],
      })

      return {points}
    }, this.setShapes)
  }

  static get touchEvents() {
    return ['touchstart', 'touchmove', 'touchcancel', 'touchend']
  }

  /**
   * The pointer's SVG coordinates using CTM (Current Transformation Matrix).
   * @see https://developer.mozilla.org/en-US/docs/Web/API/SVGGraphicsElement#Methods
   */
  getPointerCoordinates = event => {
    const ctm = this.refs.canvas.getScreenCTM()

    /**
     * When the event is triggered by a touch action, we need to determine
     * the coordinates using a different method.
     */
    const positionEvent = Canvas.touchEvents.includes(event.type) ? event.touches[0] : event
    return {
      x: (positionEvent.clientX - ctm.e) / ctm.a,
      y: (positionEvent.clientY - ctm.f) / ctm.d,
    }
  }

  /**
   * Based on these three points, two additional shapes are drawn:
   *  1. a blue parallelogram, having three of its corners in the points selected by the user.
   *  2. a yellow circle, with the same area and centre of mass as the parallelogram.
   */
  setShapes = () => {
    // We can't find a parallelogram if we don't have at least 3 of its shapes.
    if (this.state.points.length < 3) {
      return
    }

    const points = this.findParallelogram(this.state.points)
    const area = calculateArea(points)
    const circle = calculateCenterOfMass(points)

    this.setState({points, circle, area})
  }

  findParallelogram = prevPoints => {
    const undefinedIndex = prevPoints.findIndex(point => point === undefined)
    // If we only have 3 angles, we are missing the last one (0-indexed at 3).
    const missingIndex = prevPoints.length === 3 ? 3 : undefinedIndex
    const missingpoint = calculateMissingAngle(prevPoints, missingIndex)

    // Insert an item into the list at the specified index.
    const points = update(prevPoints, {
      $splice: [[missingIndex, 1, missingpoint]],
    })

    return points
  }

  /**
   * @see https://developer.mozilla.org/en-US/docs/Web/API/MouseEvent/button#Syntax
   */
  static get buttonType() {
    return {
      main: 0,
    }
  }

  handleReset = event => {
    if (event.button !== Canvas.buttonType.main) {
      return
    }

    this.setState(Canvas.initialState)
  }

  /**
   * The points attribute defines a list of points required to draw the <polygon> element.
   * @see https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/points
   * @param {{x, y}[]} points
   * @returns {String} concatenated coordinates.
   */
  static getParallelogramSVGPoints = points => {
    return points
      .reduce((accumulator, point) => {
        const coordinates = point === undefined ? '' : `${point.x},${point.y}`
        return `${accumulator} ${coordinates}`
      }, '')
      .trim()
  }

  static formatNumber = number => number.toLocaleString(undefined, {maximumFractionDigits: 2})

  render() {
    const {points, circle, area} = this.state
    const {formatNumber} = Canvas
    const parallelogramPoints = Canvas.getParallelogramSVGPoints(points)

    return (
      <React.Fragment>
        <Head />

        <main className="main">
          <header className="header">
            <h1>Shapes</h1>
          </header>
          <nav className="navigation" role="navigation">
            <a
              href="https://gitlab.com/rebelliard/shapes/blob/master/README.md"
              target="_blank"
              rel="noreferrer noopener"
            >
              About
            </a>
          </nav>
          <section className="canvas__container">
            <svg
              className="canvas"
              ref="canvas"
              onPointerDown={this.handleCanvasPointerDown}
              onPointerMove={this.handleCanvasPointerMove}
              onPointerUp={this.handleCanvasPointerUp}
              onTouchStart={this.handleCanvasPointerDown}
              onTouchMove={this.handleCanvasPointerMove}
              onTouchCancel={this.handleCanvasPointerUp}
              onTouchEnd={this.handleCanvasPointerUp}
            >
              {points.map((point, index) => (
                /**
                 * We don't filter out `undefined` points -i.e., ones being calculated-
                 * because removing the element breaks the `pointermove` event on some browsers.
                 */
                <circle
                  className="point"
                  cx={point && point.x}
                  cy={point && point.y}
                  r={Canvas.pointerRadius}
                  key={index}
                  data-index={index}
                >
                  {point && <title>{`(${formatNumber(point.x)}, ${formatNumber(point.y)})`}</title>}
                </circle>
              ))}
              {parallelogramPoints && (
                <polygon className="parallelogram" points={parallelogramPoints} />
              )}
              {circle && (
                <circle className="parallelogram__circle" cx={circle.x} cy={circle.y} r={area / 2}>
                  <title>{`(${formatNumber(circle.x)}, ${formatNumber(circle.y)})`}</title>
                </circle>
              )}
            </svg>
          </section>
          <aside className="info">
            {!!points.length && (
              <React.Fragment>
                <h2>Coordinates</h2>
                <ol>
                  {points.filter(point => point !== undefined).map((point, index) => (
                    <li key={index}>
                      (<span title="X-coordinate">{formatNumber(point.x)}</span>,{' '}
                      <span title="Y-coordinate">{formatNumber(point.y)}</span>)
                    </li>
                  ))}
                </ol>

                {area && (
                  <React.Fragment>
                    <h2>Area</h2>
                    <ul>
                      <li>Area: {formatNumber(area)}px</li>
                    </ul>
                  </React.Fragment>
                )}

                <button type="reset" className="button--reset" onPointerDown={this.handleReset}>
                  Reset
                </button>
              </React.Fragment>
            )}
          </aside>
        </main>
      </React.Fragment>
    )
  }
}

export default Canvas

import Document, {Head, Main, NextScript} from 'next/document'

/**
 * Next.js + SASS.
 * @see https://github.com/zeit/next-plugins/tree/master/packages/next-sass
 */
export default class MyDocument extends Document {
  render() {
    return (
      <html>
        <Head>
          <link rel="stylesheet" href="/_next/static/style.css" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}

import Head from 'next/head'
import React from 'react'

const index = () => {
  return (
    <Head>
      <title>Shapes | rafael.do</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" key="viewport" />
      <link rel="icon" type="image/png" href="static/favicon.png" key="icon" />
    </Head>
  )
}

export default index

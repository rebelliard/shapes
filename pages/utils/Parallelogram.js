/**
 * Find the area of a parallelogram.
 * @description
 * The shoelace formula or shoelace algorithm (also known as Gauss's area formula and
 * the surveyor's formula) is a mathematical algorithm to determine the area of a
 * simple polygon whose vertices are described by their Cartesian coordinates in the plane.
 * @see https://artofproblemsolving.com/wiki/index.php?title=Shoelace_Theorem
 * @see https://www.cut-the-knot.org/pythagoras/proof91.shtml
 *
 * @param {{x: Number, y: Number}[]} points
 * @returns {Number} area of the polygon.
 */
export const calculateArea = points => {
  let area = 0
  for (let i = 0; i < points.length; i++) {
    const j = (i + 1) % points.length
    area += points[i].x * points[j].y
    area -= points[j].x * points[i].y
  }
  return Math.sqrt(Math.abs(area) / 2)
}

/**
 * Find the fourth angle of a parallelogram.
 * @see https://www.mathplanet.com/education/geometry/quadrilaterals/properties-of-parallelograms
 *
 * @param {{x: Number, y: Number}[]} points
 * @param {Number} missingIndex
 * @returns {{x: Number, y: Number}} calculated point.
 */
export const calculateMissingAngle = (points, missingIndex) => {
  // A parallelogram has the following property: AB = DC.
  const point = {
    // ∴ A = D - C + B
    0: axis => points[3][axis] - points[2][axis] + points[1][axis],
    // ∴ B = A + C - D
    1: axis => points[0][axis] + points[2][axis] - points[3][axis],
    // ∴ C = D - A + B
    2: axis => points[3][axis] - points[0][axis] + points[1][axis],
    // ∴ D = A + C - B
    3: axis => points[0][axis] + points[2][axis] - points[1][axis],
  }

  return {
    x: point[missingIndex]('x'),
    y: point[missingIndex]('y'),
  }
}

/**
 * Find the intersection point of the diagonals.
 * @see http://onlinemschool.com/math/formula/parallelogram/ (basic property #8)
 *
 * @param  {{x: Number, y: Number}[]} parallelogram
 */
export const calculateCenterOfMass = parallelogram => {
  return {
    x: (parallelogram[0].x + parallelogram[2].x) / 2,
    y: (parallelogram[1].y + parallelogram[3].y) / 2,
  }
}

const withSass = require('@zeit/next-sass')

/**
 * Next.js + SASS.
 * @see https://github.com/zeit/next-plugins/tree/master/packages/next-sass
 */
module.exports = withSass({
  /**
   * Static HTML export.
   * @see https://github.com/zeit/next.js/#static-html-export
   */
  exportPathMap: function(defaultPathMap) {
    return {
      '/': {page: '/'},
    }
  },
})
